'use strict'
module.exports = {
  NODE_ENV: '"production"',
  VUE_APP_LOGIN_URL: '"https://istem-platform-api.azurewebsites.net/api/v1/user/login"',
  VUE_APP_ACCOMMODATION_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"',
  VUE_APP_INSTITUTE_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"',
  VUE_APP_USER_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"'
}
