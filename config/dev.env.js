'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  // VUE_APP_LOGIN_URL: '"http://localhost:3000/api/v1/user/login"',
  // VUE_APP_ACCOMMODATION_BASE_URL: '"http://localhost:3000/api/v1"',
  // VUE_APP_INSTITUTE_BASE_URL: '"http://localhost:3000/api/v1"',
  // VUE_APP_USER_BASE_URL: '"http://localhost:3000/api/v1"'
  VUE_APP_LOGIN_URL: '"https://istem-platform-api.azurewebsites.net/api/v1/user/login"',
  VUE_APP_ACCOMMODATION_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"',
  VUE_APP_INSTITUTE_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"',
  VUE_APP_USER_BASE_URL: '"https://istem-platform-api.azurewebsites.net/api/v1"'
})
