import api from './index';
import URL from '../../constants/url';

export default {
  getInstitute: params => api().get(URL.buildUrl('GET_INSTITUTE', params)),
  updateInstitute: (instituteId, params) => api().put(URL.buildUrl('UPDATE_INSTITUTE', { instituteId }), params),
  addInstitute: params => api().post(URL.ADD_INSTITUTE, params),
  getOnboardings: instituteId => api().get(URL.buildUrl('GET_INSTITUTE_ONBOARDINGS', { instituteId })),
  addOnboarding: (instituteId, params) => api().post(URL.buildUrl('ADD_ONBOARDING', { instituteId }), params),
  getOnboarding: (instituteId, onboardingId) => api().get(URL.buildUrl('GET_INSTITUTE_ONBOARDING', {
    instituteId,
    onboardingId
  })),
  editOnboarding: (instituteId, onboardingId, params) => api().put(URL.buildUrl('EDIT_ONBOARDING', {
    instituteId,
    onboardingId
  }), params),
  getJobTitles: () => api().get(URL.buildUrl('GET_JOB_TITLE'))
};
