import Axios from 'axios';
import Vue from 'vue';

// Axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('sid')}`;
// Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

const instance = Axios.create({
  // baseURL: `http://api.pearson.com/v2/dictionaries`,
  // withCredentials: false,
  headers: {
    Accept: 'application/json',
    Authorization: `Bearer ${localStorage.getItem('sid')}`,
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  }
});


instance.interceptors.request.use((config) => {
  // Do something before request is sent
  // console.log('Start Ajax Call');
  document.getElementsByClassName('vue-ui-loading-indicator')[0].setAttribute('aria-hidden', 'false');
  return config;
}, (error) => {
  // Do something with request error
  Vue.$log.error(error);
  return Promise.reject(error);
});

instance.interceptors.response.use((response) => {
  // Do something with response data
  // console.log('Done with Ajax call');
  document.getElementsByClassName('vue-ui-loading-indicator')[0].setAttribute('aria-hidden', 'true');
  return response;
}, (error) => {
  // Do something with response error
  Vue.$log.error(error);
  return Promise.reject(error);
});

export default() => instance;
