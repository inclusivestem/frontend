import api from './index';
import URL from '../../constants/url';

export default {
  getUsers: params => api().get(URL.buildUrl('GET_USERS', {}, params)),
  updateUser: (userId, params) => api().put(URL.buildUrl('UPDATE_USER', { userId }), params),
  inviteUser: params => api().post(URL.buildUrl('INVITE_USER'), params)
};
