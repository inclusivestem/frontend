import api from './index';
import URL from '../../constants/url';

export default {
  getAccommodations: params => api().get(URL.buildUrl('GET_ACCOMMODATIONS', {}, params))
};
