// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'vue-toast-notification/dist/index.css';
import 'vue-tour/dist/vue-tour.css';

import BootstrapVue from 'bootstrap-vue';
import VueToast from 'vue-toast-notification';
import VueTour from 'vue-tour';

import Vue from 'vue';
import Axios from 'axios';

import './middlewares/logger';
import './middlewares/auth';
import App from './App';
import router from './router';
import store from './store';
import ability from './middlewares/ability';
import utils from './utils';

Vue.prototype.$http = Axios;
Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VueToast, {
  // One of options
  position: 'bottom'
});
Vue.use(VueTour);

if ((localStorage.getItem('sid') === null || localStorage.getItem('sid') === '') &&
  utils.getQueryStringValue('SID') === '') {
  location = `${process.env.VUE_APP_LOGIN_URL}?redirect=${window.location.href}`;
} else if(utils.getQueryStringValue('SID') !== "") {
  localStorage.setItem('sid', utils.getQueryStringValue('SID'));
}

Axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('sid')}`;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  ability,
  components: { App },
  template: '<App/>'
});
