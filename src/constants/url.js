/**
 * Created by Himanshu
 */
const uritemplate = require('uritemplate');

const ACCOMMODATION_BASE_URL = process.env.VUE_APP_ACCOMMODATION_BASE_URL;
const INSTITUTE_BASE_URL = process.env.VUE_APP_INSTITUTE_BASE_URL;
const USER_BASE_URL = process.env.VUE_APP_USER_BASE_URL;

// above Can be done using axios also but kept here to future scope of multiple microservice baseurl

const makeQueryParam = (queryParamMap = {}) => {
  let string = '?';
  let paramList;
  let index;
  if (queryParamMap) {
    Object.keys(queryParamMap).forEach((key) => {
      if (queryParamMap[key].constructor === Array) {
        paramList = queryParamMap[key];
        index = paramList.length;
        while (index) {
          index -= 1;
          string += `${key}[]=${paramList[index]}&`;
        }
      } else {
        string += `${key}=${queryParamMap[key]}&`;
      }
    });
  }
  return string;
};

export default {
  GET_ACCOMMODATIONS: `${ACCOMMODATION_BASE_URL}/accommodation`,
  ADD_INSTITUTE: `${INSTITUTE_BASE_URL}/institute`,
  GET_INSTITUTE: `${INSTITUTE_BASE_URL}/institute/{instituteId}`,
  UPDATE_INSTITUTE: `${INSTITUTE_BASE_URL}/institute/{instituteId}`,
  GET_INSTITUTE_ONBOARDINGS: `${INSTITUTE_BASE_URL}/institute/{instituteId}/onboarding`,
  ADD_ONBOARDING: `${INSTITUTE_BASE_URL}/institute/{instituteId}/onboarding`,
  GET_INSTITUTE_ONBOARDING: `${INSTITUTE_BASE_URL}/institute/{instituteId}/onboarding/{onboardingId}`,
  EDIT_ONBOARDING: `${INSTITUTE_BASE_URL}/institute/{instituteId}/onboarding/{onboardingId}`,
  GET_JOB_TITLE: `${INSTITUTE_BASE_URL}/institute/job-title`,
  UPDATE_USER: `${USER_BASE_URL}/user/{userId}`,
  INVITE_USER: `${USER_BASE_URL}/user`,
  GET_USERS: `${USER_BASE_URL}/user`,
  buildUrl(urlName, params, queryParam) {
    return uritemplate.parse(this[urlName]).expand(params) + makeQueryParam(queryParam);
  }
};
