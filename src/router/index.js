import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/components/Home';

import AccommodationList from '@/components/AccommodationList';
import Disability from '@/components/Disability';
import Committee from '@/components/Committee';
import CommitteeList from '@/components/CommitteeList';
import Institute from '@/components/Institute';
import InstituteProfile from '@/components/InstituteProfile';
import Onboarding from '@/components/Onboarding';
import OnboardingList from '@/components/OnboardingList';
import Staff from '@/components/Staff';
import RequestInstance from '@/components/RequestInstance';
import RequestFormList from '@/components/RequestFormList';

Vue.use(Router);

export default new Router({
  mode: 'history',
  // linkActiveClass: 'active',
  linkExactActiveClass: 'exact-active active',
  routes: [
    { path: '/institute',
      name: 'Institute',
      component: Institute,
      children: [
        { path: '', name: 'InstituteProfile', component: InstituteProfile },
        { path: 'accommodation', name: 'AccommodationList', component: AccommodationList },
        { path: 'disability', name: 'Disability', component: Disability },
        { path: 'committee', name: 'CommitteeList', component: CommitteeList },
        { path: 'committee/new', name: 'Committee', component: Committee },
        { path: 'committee/:committeeId', name: 'CommitteeId', component: Committee, props: true },
        { path: 'onboarding', name: 'OnboardingList', component: OnboardingList },
        { path: 'onboarding/new', name: 'Onboarding', component: Onboarding },
        { path: 'onboarding/:onboardingId', name: 'OnboardingId', component: Onboarding, props: true },
        { path: 'staff', name: 'Staff', component: Staff },
        { path: 'request/new',name:'Request',component: RequestInstance},
        { path: 'request',name:'RequestFormList',component:RequestFormList}
      ] },
    { path: '/', name: 'Home', component: Home }
    // { path: '/institute/committee', name: 'Committee', component: Committee }
  ]
});
