
const parseJwt = (token) => {
  const base64Url = token.split('.')[1];
  if (typeof base64Url === 'undefined') {
    location = `${process.env.VUE_APP_LOGIN_URL}?redirect=${window.location.href}`;
  }
  const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
  return JSON.parse(window.atob(base64));
};

const updateJwt = (sid, instituteId) => {
  const jsObj = parseJwt(sid);
  const sidSplit = sid.split('.');
  jsObj.instituteId = instituteId;
  sidSplit[1] = window.btoa(JSON.stringify(jsObj));
  localStorage.setItem('sid', sidSplit.join('.'));
  return jsObj;
};

export default {
  parseJwt,
  getInstituteId() {
    return parseJwt(localStorage.getItem('sid')).instituteId;
  },
  getUserId() {
    return parseJwt(localStorage.getItem('sid')).id;
  },
  updateInstituteId(instituteId) {
    return updateJwt(localStorage.getItem('sid'), instituteId);
  },
  getCookieValue(a) {
    const b = document.cookie.match(`(^|[^;]+)\\s*${a}\\s*=\\s*([^;]+)`);
    return b ? b.pop() : '';
  },
  getQueryStringValue(key) {
    return decodeURIComponent(window.location.search.replace(
      new RegExp(`^(?:.*[&\\?]${encodeURIComponent(key)
        .replace(/[.+*]/g, '\\$&')}(?:\\=([^&]*))?)?.*$`, 'i'), '$1'));
  },
  isEmpty(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object;
  },
  objectFlip(obj) {
    const ret = {};
    Object.keys(obj).forEach((key) => {
      ret[obj[key]] = key;
    });
    return ret;
  }
};
