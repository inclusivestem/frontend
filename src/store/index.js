import Vue from 'vue';
import Vuex from 'vuex';
import { accommodation } from './modules/accommodation';
import { disability } from './modules/disability';
import { institute } from './modules/institute';
import { user } from './modules/user';
import { request } from './modules/request';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    accommodation,
    disability,
    institute,
    request,
    user
  }
});
