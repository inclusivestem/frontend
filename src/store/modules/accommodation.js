import Vue from 'vue';
import AccommodationService from '@/services/api/accommodation';

import utils from '../../utils';

const serviceTypeMap = {
  'INCLUSIVE EXAMINATIONS': 'examAccommodations',
  'NOTE TAKING AND IN CLASS ASSISTANCE OPTIONS': 'classAssistanceOptions',
  'ACCESSIBLE CONTENT SERVICES': 'accessibleContentServices',
  'TEACHING LEARNING SUPPORT': 'teachingSupport',
  'ACCESSIBLE INFRASTRUCTURE SERVICES': 'accessibleInfrastructure'
};


const arrayToObject = (array, accIds) => {
  const ret = {};
  Object.entries(serviceTypeMap).forEach(([, value]) => {
    ret[value] = {};
  });
  array.map((item) => {
    if (!accIds || accIds.includes(item.id)) {
      if (!ret[serviceTypeMap[item.serviceType]][item.disability]) {
        ret[serviceTypeMap[item.serviceType]][item.disability] = {};
      }
      const acc = Object.assign({}, item);
      acc.type = 'checkbox';
      if (ret[serviceTypeMap[item.serviceType]][item.disability][item.type]) {
        ret[serviceTypeMap[item.serviceType]][item.disability][item.type].push(acc);
      } else {
        ret[serviceTypeMap[item.serviceType]][item.disability][item.type] = [acc];
      }
    }
    return item;
  });
  return ret;
};

// eslint-disable-next-line import/prefer-default-export
export const accommodation = {
  state: {
    accommodation: null,
    accommodations: [],
    serviceTypes: utils.objectFlip(serviceTypeMap)
  },
  getters: {
    getAccommodation: state => state.accommodation,
    getAccommodations: state => state.accommodations,
    getServiceTypes: state => state.serviceTypes,
    getAccommodationsTree: state => arrayToObject(state.accommodations),
    getSelectedAccommodationsTree: (state, getters, rootState) => arrayToObject(state.accommodations, rootState.institute.institute.availableAccommodationIds)
  },
  mutations: {
    setAccommodation(state, acc) {
      state.accommodation = acc;
    },
    setAccommodations(state, accommodations) {
      state.accommodations = accommodations;
    }
  },
  actions: {
    getAccommodations: ({ commit }, query) => {
      AccommodationService.getAccommodations(query)
        .then((response) => {
          Vue.$log.info(arrayToObject(response.data.data));
          commit('setAccommodations', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    }
  }
};
