
import Vue from 'vue';
import UserService from '@/services/api/user';

// eslint-disable-next-line import/prefer-default-export
export const user = {
  state: {
    user: null,
    users: null,
    filteredUsers: [],
    members: []
  },
  getters: {
    getFilteredUsers: state => state.filteredUsers,
    getMembers: state => state.members
  },
  mutations: {
    setUsers(state, users) {
      state.users = users;
    },
    setFilteredUsers(state, users) {
      state.filteredUsers = users;
    },
    setMembers(state, users) {
      state.members = users;
    },
    removeMember(state, u) {
      state.members = state.members.filter(x => x.id !== u.id);
    }
  },
  actions: {
    getUsers: ({ commit }, query) => {
      UserService.getUsers(query)
        .then((response) => {
          commit('setFilteredUsers', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    getMembers: ({ commit }, query) => {
      UserService.getUsers(query)
        .then((response) => {
          commit('setMembers', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    updateUser: ({ commit }, payload) => {
      UserService.updateUser(payload.id, payload)
        .then((response) => {
          Vue.$log.info(response);
          Vue.$log.info(commit);
          Vue.$toast.success('Details saved');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    inviteUser: ({ commit }, payload) => {
      UserService.inviteUser(payload)
        .then((response) => {
          Vue.$log.info(response);
          Vue.$log.info(commit);
          Vue.$toast.success('User invite');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    removeMember: ({ commit, dispatch }, payload) => {
      dispatch('updateUser', payload);
      commit('removeMember', payload);
    },
    logoutUser:()=>{
      localStorage.removeItem("sid");
    }
  }
};
