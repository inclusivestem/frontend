import Vue from 'vue';
//import InstituteService from '@/services/api/request';
import utils from '../../utils';

const mandatoryFields = [
  { type: 'text',
    name: 'Student Assigned Number(UID/ROLLNO)',
    shortDescription: '',
    required: true },
  { type: 'text',
    name: 'Year',
    shortDescription: 'Class of study, if a new student, enter the year of study you will be joining',
    required: true },
  { type: 'text',
    name: 'Institute',
    shortDescription: 'Name of your Institute',
    required: true },
  { type: 'text',
    name: 'Major',
    shortDescription: 'Program of Study',
    required: true },
  { type: 'text',
    name: 'Email Id',
    shortDescription: '',
    required: true },
  { type: 'text',
    name: 'Contact No',
    shortDescription: '',
    required: true }
    ];

const approvedFields=[
	{ type: 'checkbox',
    name: '1.Note Taking and In Class Room Support: Please fill details',
    shortDescription: '',
    options:['You would require Note takers assigned for which classes?', 
	'Tell us if you have any specific need in the note taking volunteer (Gender, Specific knowledge that you would like the person to have):',
		'You would require Lab Assistant assigned for which Labs?',
	'Tell us if you have any specific need in the lab assistant volunteer (Gender, Specific knowledge that you would like the person to have):'], 
    required: true },
    { type: 'checkbox',
    name: '2.Please state which of the following formats would you like content to be made available for you and please mention the course codes/names for each format type: (e.g. E copy for English, Tactile Diagrams for Biology)',
    shortDescription: '',
    options:['E cop','Audio Files','Easy to Read Content','Tactile Diagrams of Images','Audio Visuals to be subtitled/transcript'],
    required: true },
    { type: 'checkbox',
    name: '3.Please select which of these teaching support options you would like: ',
    shortDescription: '',
    options:['Which classes would you like to have a teaching assistant assigned for out of class reinforcement and learning?',
	'How often would you like the support pf a teaching assistant. Mention specifically for each subject/Project/assignments/presentations. E.g Twice a week 2 hours each for mathematics, once a week 1 hour for English, twice a week 1 hour each for project work'
	,'How often would you like the support of a special educator? Mention specifically which skills would you need support for'],
    required: true },
    { type: 'text',
    name: '4.For your Examinations, if you have opted for scribes please let us know would you bring your own scribe or would you like us to arrange? Please mention the courses for which you would like us to arrange for the scribe.',
    shortDescription: '',
    required: true }
  
	];

export const request = {
	state:{
	requests:[],
	request:{
		mandatoryFields,
		approvedFields,
		dynamicFields: [{ type: 'text', name: 'Miscellaneous', shortDescription: '', required: true }]
		}
	},
	getters:{
		request:state => state.request
	},
	 mutations: {
    setInstitute(state, uni) {
      state.institute = uni;
    	},
    setRequests(state, onboardings) {
      state.onboardings = onboardings;
    	},
    setRequest(state, request) {
      state.request= request;
    	},
    addRequestField(state, fields) {
      state.request.dynamicFields = [...state.request.dynamicFields, fields];
   		 },
    removeRequestField(state, index) {
      if (index > -1) {
        state.request.dynamicFields.splice(index, 1);
      		}
    	}
    }
};
