import Axios from 'axios';

Axios.defaults.headers.common.Authorization = `Bearer ${localStorage.getItem('sid')}`;
Axios.defaults.headers.common['Access-Control-Allow-Origin'] = '*';

// eslint-disable-next-line import/prefer-default-export
export const disability = {
  state: {
    disabilityTypes: [
      'Locomotor including Orthopedic Disability',
      'Leprosy Cured',
      'Cerebral Palsy',
      'Dwarfism',
      'Muscular dystrophy',
      'Acid Attack victims',
      'Blindness',
      'Low Vision',
      'Hearing impaired: deaf ',
      'Hearing Impaired: hard of hearing',
      'Speech and language disability',
      'Intellectual Disability/Slow Learners',
      'Specific learning disabilities',
      'Autism spectrum disorder',
      'Multiple disabilities including deaf-blindness',
      'Mental Illness ',
      'Multiple sclerosis',
      'Parkinson\'s disease',
      'Haemophilia ',
      'Thalassemia',
      'Sickle cell disease'
    ]
  },
  getters: {
    getDisabilityTypes: state => state.disabilityTypes,
    disabilityTypes: state => state.disabilityTypes
  },
  mutations: {
  },
  actions: {
  }
};
