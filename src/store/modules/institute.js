import Vue from 'vue';
import InstituteService from '@/services/api/institute';
import utils from '../../utils';


const mandatoryFields = [
  { type: 'text',
    name: 'Name',
    shortDescription: '',
    required: true },
  { type: 'text',
    name: 'Year',
    shortDescription: 'Class of study, if a new student, enter the year of study you will be joining',
    required: true },
  { type: 'text',
    name: 'Major',
    shortDescription: 'Program of Study',
    required: true },
  { type: 'text',
    name: 'Email id',
    shortDescription: '',
    required: true },
  { type: 'text',
    name: 'Contact No.',
    shortDescription: '',
    required: true },
  { type: 'text',
    name: 'Roll No',
    shortDescription: '',
    required: true },

];
const currentStatusFields = [
  { type: 'text',
    name: 'Current Accommodation',
    shortDescription: 'List the accommodations taken for the last exam taken.',
    required: true },
  { type: 'checkbox',
    name: 'Your preferred way of reading books and textual content:',
    shortDescription: '',
    options: ['Braille', 'Large Font', 'Ink Print Books', 'Audio Books', 'E Books',
      'Human Readers', 'simplified', 'bulleted content', 'Any Other'],
    required: true },
  { type: 'checkbox',
    name: 'Your preferred way of writing:',
    shortDescription: '',
    options: ['Braille', 'Computer with screen readers', 'Computer with magnifiers', 'Regular Computer',
      'Regular pen and paper writing', 'large font writing', 'writing with magnification',
      'dictating to someone', 'Any Other'],
    required: true },
  { type: 'checkbox',
    name: ' Your preferred way of studying content:',
    shortDescription: '',
    options: ['Self-study', 'study in groups', 'study with a tutor for explanation', 'Any Other'],
    required: true }
];


// eslint-disable-next-line import/prefer-default-export
export const institute = {
  state: {
    institute: { committees: [], availableAccommodationIds: [], disabilityTypes: [] },
    onboardings: [],
    onboarding: {
      mandatoryFields,
      currentStatusFields,
      dynamicFields: [{ type: 'text', name: 'Miscellaneous', shortDescription: '', required: true }]
    },
    jobTitles: []
  },
  getters: {
    institute: state => state.institute,
    onboardings: state => state.onboardings,
    onboarding: state => state.onboarding,
    committee: state => (id) => {
      /* eslint no-underscore-dangle: 0 */
      if (id && state.institute.committees.length > 0) {
        return state.institute.committees.find(comm => comm._id === id);
      }
      return {};
    }
  },
  mutations: {
    setInstitute(state, uni) {
      state.institute = uni;
    },
    setOnboardings(state, onboardings) {
      state.onboardings = onboardings;
    },
    setOnboarding(state, onboarding) {
      state.onboarding = onboarding;
    },
    addOnboardingField(state, fields) {
      state.onboarding.dynamicFields = [...state.onboarding.dynamicFields, fields];
    },
    removeOnboardingField(state, index) {
      if (index > -1) {
        state.onboarding.dynamicFields.splice(index, 1);
      }
    },
    setJobTitles(state, jobTitles) {
      state.jobTitles = jobTitles;
    }
  },
  actions: {
    getInstitute: ({ commit }) => {
      if (utils.getInstituteId()) {
        InstituteService.getInstitute({ instituteId: utils.getInstituteId() })
          .then((response) => {
            commit('setInstitute', response.data.data);
          })
          .catch((error) => {
            Vue.$log.error(error.response);
          });
      }
    },
    updateInstitute: ({ commit, state }) => {
      InstituteService.updateInstitute(utils.getInstituteId(), state.institute)
        .then((response) => {
          commit('setInstitute', response.data.data);
          Vue.$toast.success('Details saved');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
          Vue.$toast.error('Error saving data');
        });
    },
    addInstitute: ({ dispatch, commit, state }) => {
      InstituteService.addInstitute(state.institute)
        .then((response) => {
          commit('setInstitute', response.data.data);
          utils.updateInstituteId(response.data.data.id);

          dispatch('updateUser', {
            id: utils.getUserId(),
            instituteId: response.data.data.id
          }, { root: true });

          // localStorage.setItem('institute.id', response.data.data.id);
          Vue.$toast.success('Institute details saved');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
          Vue.$toast.error('Error saving data');
        });
    },
    addCommittee: ({ dispatch, state }, committee) => {
      state.institute.committees.push(committee);
      dispatch('updateInstitute', state.institute.id, state.institute);
    },
    getOnboardings: ({ commit }) => {
      InstituteService.getOnboardings(utils.getInstituteId())
        .then((response) => {
          commit('setOnboardings', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    addOnboarding: ({ commit, state }) => {
      state.onboarding.instituteId = utils.getInstituteId();
      InstituteService.addOnboarding(utils.getInstituteId(), state.onboarding)
        .then((response) => {
          commit('setOnboarding', response.data.data);
          Vue.$toast.success('Onboarding form added');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
          Vue.$toast.error('Error saving form');
        });
    },
    getOnboarding: ({ commit }, onboardingId) => {
      InstituteService.getOnboarding(utils.getInstituteId(), onboardingId)
        .then((response) => {
          commit('setOnboarding', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    },
    editOnboarding: ({ commit, state }, onboardingId) => {
      InstituteService.editOnboarding(utils.getInstituteId(), onboardingId, state.onboarding)
        .then((response) => {
          commit('setOnboarding', response.data.data);
          Vue.$toast.success('Onboarding form updated');
        })
        .catch((error) => {
          Vue.$log.error(error.response);
          Vue.$toast.error('Error saving form');
        });
    },
    getJobTitles: ({ commit }) => {
      InstituteService.getJobTitles()
        .then((response) => {
          commit('setJobTitles', response.data.data);
        })
        .catch((error) => {
          Vue.$log.error(error.response);
        });
    }
  }
};
